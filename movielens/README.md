Movielens Project

All files provided as per the capstone course instruction:

"The submission for the MovieLens project will be three files:
- a report in the form of an Rmd file,
- a report in the form of a PDF document knit from your Rmd file, and
- an R script that generates your predicted movie ratings and calculates RMSE.
The R script should contain all of the code and comments for your project."

Please note that scripts have been adapted for use with R 4.0 or later
(if you use with R 3.6 or earlier please adapt the data preparation part which
was provided by the course team according to the comments). 


The files can also be found online under:
https://gitlab.com/lueckt/capstone --> folder movielens

-->movielens.Rmd: R markup file for the report

-->movielens.pdf: report created from movielens.Rmd via knit

-->movielens.R: R script (which includes ALL code included in movielens.Rmd, see instruction)
- If you are only interested in the training and prediction of the final
model you can delete any code used for explanatory purposes in the report
between the SNIP and SNAP comments.

-->movielens_SHORT.R (only included in gitlab repository, not uploaded to edx): short version
of movielens.R where the section between SNIP and SNAP is deleted, i.e. only the
training and prediction part of the final model is included.
